import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _firstTitleController = TextEditingController();
  TextEditingController _secondTitleController = TextEditingController();
  TextEditingController _thirdTitleController = TextEditingController();
  TextEditingController _firstBtnTxtController = TextEditingController();
  TextEditingController _secondBtnTxtController = TextEditingController();
  TextEditingController _thirdBtnTxtController = TextEditingController();
  late String _chosenFirstOption, _chosenSecondOption, _chosenThirdOption;

  @override
  void initState() {
    super.initState();
    _firstTitleController.text = 'Premium';
    _secondTitleController.text = 'ANP';
    _thirdTitleController.text = 'Renewal';
    _firstBtnTxtController.text = 'Clear';
    _secondBtnTxtController.text = 'More';
    _thirdBtnTxtController.text = 'Detail';
    _chosenFirstOption = _chosenSecondOption = 'Button';
    _chosenThirdOption  = 'Progress Bar';

  }


  @override
  void dispose() {
    super.dispose();
    _firstTitleController.dispose();
    _secondTitleController.dispose();
    _thirdTitleController.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _showBody(),
    );
  }

  Widget _showBody() {
    return ListView(
      children: [
        _buildContent(),
        _changeFirstBlock(),
        _changeSecondBlock(),
        _changeThirdBlock()
      ],
    );
  }

  Widget _buildContent(){
    return Container(
      height: MediaQuery.of(context).size.height * .3,
      padding: const EdgeInsets.all(15.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: ListView(
          padding: const EdgeInsets.all(8.0),
          children: [
            _showTitle(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Expanded(child: _buildFirstCards(title: '${_firstTitleController.text}', type: _chosenFirstOption, buttonText: _firstBtnTxtController.text)),
                  Expanded(child: _buildSecondCards(title: '${_secondTitleController.text}', type: _chosenSecondOption, buttonText: _secondBtnTxtController.text)),
                  Expanded(child: _buildThirdCards(title: '${_thirdTitleController.text}', type: _chosenThirdOption, buttonText: _thirdBtnTxtController.text))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _changeFirstBlock(){
    return Container(
      height: MediaQuery.of(context).size.height * .4,
      padding: const EdgeInsets.all(15.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: ListView(
          padding: const EdgeInsets.all(15.0),
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                'Change First Block',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    color: Colors.blueAccent),
              ),
            ),
            TextFormField(
              controller: _firstTitleController,
              // initialValue: 'Premium',
              decoration: InputDecoration(
                labelText: 'Set Title',
              ),
              autofocus:false,
              onChanged: (String val){
                setState(() {
                  _firstTitleController.text = val;
                  _firstTitleController.selection = TextSelection.fromPosition(TextPosition(offset: _firstTitleController.text.length));
                });
              },
            ),
            SizedBox(height: 30,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Set widget option: '),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownButton<String>(
                    value: _chosenFirstOption,
                    //elevation: 5,
                    style: TextStyle(color: Colors.black),

                    items: <String>[
                      'Button',
                      'Progress Bar',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (String? value) {
                      setState(() {
                        _chosenFirstOption = value!;
                      });
                    },
                  ),
                ),
              ],
            ),
            (_chosenFirstOption == 'Button')?
            TextFormField(
              controller: _firstBtnTxtController,
              // initialValue: 'Premium',
              decoration: InputDecoration(
                labelText: 'Set button text',
              ),
              autofocus:false,
              onChanged: (String val){
                setState(() {
                  _firstBtnTxtController.text = val;
                  _firstBtnTxtController.selection = TextSelection.fromPosition(TextPosition(offset: _firstBtnTxtController.text.length));
                });
              },
            ) : SizedBox(height: 0,),
          ],
        ),
      ),
    );
  }

  Widget _changeSecondBlock(){
    return Container(
      height: MediaQuery.of(context).size.height * .4,
      padding: const EdgeInsets.all(15.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: ListView(
          padding: const EdgeInsets.all(15.0),
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                'Change Second Block',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    color: Colors.blueAccent),
              ),
            ),
            TextFormField(
              controller: _secondTitleController,
              decoration: InputDecoration(
                labelText: 'Set Title',
              ),
              autofocus:false,
              onChanged: (String val){
                setState(() {
                  _secondTitleController.text = val;
                  _secondTitleController.selection = TextSelection.fromPosition(TextPosition(offset: _secondTitleController.text.length));
                });
              },
            ),
            SizedBox(height: 30,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Set widget option: '),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownButton<String>(
                    value: _chosenSecondOption,
                    //elevation: 5,
                    style: TextStyle(color: Colors.black, fontSize: 15),
                    items: <String>[
                      'Button',
                      'Progress Bar',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (String? value) {
                      setState(() {
                        _chosenSecondOption = value!;
                      });
                    },
                  ),
                ),
              ],
            ),
            (_chosenSecondOption == 'Button')?
            TextFormField(
              controller: _secondBtnTxtController,
              // initialValue: 'Premium',
              decoration: InputDecoration(
                labelText: 'Set button text',
              ),
              autofocus:false,
              onChanged: (String val){
                setState(() {
                  _secondBtnTxtController.text = val;
                  _secondBtnTxtController.selection = TextSelection.fromPosition(TextPosition(offset: _secondBtnTxtController.text.length));
                });
              },
            ) : SizedBox(height: 0,),
          ],
        ),
      ),
    );
  }



  Widget _changeThirdBlock(){
    return Container(
      height: MediaQuery.of(context).size.height * .4,
      padding: const EdgeInsets.all(15.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: ListView(
          padding: const EdgeInsets.all(15.0),
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                'Change Third Block',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    color: Colors.blueAccent),
              ),
            ),
            TextFormField(
              controller: _thirdTitleController,
              // initialValue: 'Premium',
              decoration: InputDecoration(
                labelText: 'Set Title',
              ),
              autofocus:false,
              onChanged: (String val){
                setState(() {
                  _thirdTitleController.text = val;
                  _thirdTitleController.selection = TextSelection.fromPosition(TextPosition(offset: _thirdTitleController.text.length));
                });
              },
            ),
            SizedBox(height: 30,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Set widget option: '),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownButton<String>(
                    value: _chosenThirdOption,
                    //elevation: 5,
                    style: TextStyle(color: Colors.black, fontSize: 15),
                    items: <String>[
                      'Button',
                      'Progress Bar',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (String? value) {
                      setState(() {
                        _chosenThirdOption = value!;
                      });
                    },
                  ),
                ),
              ],
            ),
            SizedBox(height: 30,),
            (_chosenThirdOption == 'Button')?
            TextFormField(
              controller: _thirdBtnTxtController,
              // initialValue: 'Premium',
              decoration: InputDecoration(
                labelText: 'Set button text',
              ),
              autofocus:false,
              onChanged: (String val){
                setState(() {
                  _thirdBtnTxtController.text = val;
                  _thirdBtnTxtController.selection = TextSelection.fromPosition(TextPosition(offset: _thirdBtnTxtController.text.length));
                });
              },
            ) : SizedBox(height: 0,),
          ],
        ),
      ),
    );
  }


  Widget _showTitle() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        'Business Indicator',
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25.0,
            color: Colors.blueAccent),
      ),
    );
  }

  Widget _buildFirstCards(
      {String title = '',
      String type = 'Button',
      double value = 0.2,
      Color color = Colors.red,
      String buttonText = 'Clear'}) {
    return Card(
      color: Color(0xFFECF4FB),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildTypes(title),
            SizedBox(
              height: 30,
            ),
            (type != 'Button')
                ? _textWithProgressBar(value, color)
                : _buttons(buttonText),
          ],
        ),
      ),
    );
  }

  Widget _buildSecondCards(
      {String title = '',
      String type = 'Button',
      double value = 0.3,
      Color color = Colors.yellow,
      String buttonText = 'Clear'}) {
    return Card(
      color: Color(0xFFECF4FB),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildTypes(title),
            SizedBox(
              height: 30,
            ),
            (type != 'Button')
                ? _textWithProgressBar(value, color)
                : _buttons(buttonText),
          ],
        ),
      ),
    );
  }

  Widget _buildThirdCards(
      {String title = '',
      String type = 'Progress Bar',
      double value = 0.35,
      Color color = Colors.green,
      String buttonText = 'Clear'}) {
    return Card(
      color: Color(0xFFECF4FB),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildTypes(title),
            SizedBox(
              height: 30,
            ),
            (type != 'Button')
                ? _textWithProgressBar(value, color)
                : _buttons(buttonText),
          ],
        ),
      ),
    );
  }

  Widget _buildTypes(String text) {
    return Text(
      '$text',
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 20.0,
        color: Colors.blueAccent,
      ),
    );
  }

  Widget _textWithProgressBar(double value, Color color) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${(value * 100).toInt()} L',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
                color: Colors.blueAccent),
          ),
          SizedBox(
            height: 10,
          ),
          LinearProgressIndicator(
            value: value,
            color: color,
            minHeight: 8,
            backgroundColor: Colors.white,
          )
        ],
      ),
    );
  }

  Widget _buttons(String text) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: MaterialButton(
            color: Colors.blueAccent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 5),
              child: Text(
                '$text',
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }




}
